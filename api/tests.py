from django.test import TestCase
from api.models import Supply
from api import queries


class SupplyIntegrationTest(TestCase):
    def setUp(self):
        Supply.objects.create(km_supply="10000",
                              quantity_liters=44.56,
                              value=133.24,
                              date_supply="2017-07-1 22:24:57.891964+00:00",
                              created_at="2017-07-1 22:24:57.891964+00:00")

        Supply.objects.create(km_supply="10560",
                              quantity_liters=44.56,
                              value=133.24,
                              date_supply="2017-07-14 22:24:57.891964+00:00",
                              created_at="2017-07-14 22:24:57.891964+00:00")

    def test_sum_supply(self):
        sum_liters = queries.sum_quantity_liters_supply_month('07', '2017')
        self.assertEquals(sum_liters['quantity_liters'], 89.12)

    def test_count_supply(self):
        count_supply = queries.count_supply_month('07', '2017')
        self.assertEquals(count_supply, 2)

    def test_diff_km_supply(self):
        diff_km_supply = queries.calculate_km_supply_month('07', '2017')
        self.assertEquals(diff_km_supply, 560)

    def test_km_liters(self):
        km_liters = queries.calculate_km_liters('07', '2017')
        self.assertEquals(km_liters, 6.283662477558348)
